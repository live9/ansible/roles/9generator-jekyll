#!/bin/bash
DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env
set -Eeo pipefail
_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about ${BASH_LINENO[0]}"
  docker logout ${CI_REGISTRY}
}
trap _traperr ERR

### Functions ###
_usage() {
	echo "USAGE: ${basename_} prod|preprod|feature"
	exit 1
}

if [[ $# -ne 1 ]]; then
  _usage
fi

### Variables ###
infra_env=${1}
basename_=$(basename ${0})
servicename_=$(basename ${PWD})
pwd_=${PWD}
tmp_=${DEPLOY_DIR}-back.pipeline_${CI_PIPELINE_ID}-date_$(date +%F-%H%M%S)

# Include NFS handling variables & functions
# shellcheck source=${PWD}/nfs.sh
. ${PWD}/gitlab-ci/nfs.sh

### Program ###
echo ${basename_}: start

[ -d ${DEPLOY_DIR} ] && mv ${DEPLOY_DIR} ${tmp_}
git clone ${pwd_} ${DEPLOY_DIR}
cp -r ${pwd_}/artifacts/* ${DEPLOY_DIR}/
cd ${DEPLOY_DIR}

# Add NFS shares for this app
sudo ${modify_exports_script_} -f ${exports_file_} -m ${nfs_base_path_}/${servicename_}-${infra_env}/app -o ${nfs_export_options_}
sudo ${modify_exports_script_} -f ${exports_file_} -m ${nfs_base_path_}/${servicename_}-${infra_env}/certs -o ${nfs_export_options_}
sudo mkdir -p ${nfs_base_path_}/${servicename_}-${infra_env}/{app,certs}
sudo exportfs -a
docker-compose -f docker-compose.provision.yml pull
docker-compose -f docker-compose.provision.yml up -d
# TODO: must wait until app and certs are fully provisioned by docker-compose
# doing a sleep for now.
sleep 20

echo "docker stack deploy -c docker-stack.yml --with-registry-auth ${servicename_}-${infra_env}"
docker stack deploy -c docker-stack.yml --with-registry-auth ${servicename_}-${infra_env}

docker logout ${CI_REGISTRY}
rm -fr ${tmp_}

echo ${basename_}: end
