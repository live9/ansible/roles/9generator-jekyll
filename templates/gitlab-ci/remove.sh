#!/bin/bash
DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env
set -Eeo pipefail

_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about ${BASH_LINENO[0]}"
}
trap _traperr ERR

### Functions ###
_usage() {
	echo "USAGE: ${basename_} prod|preprod|feature"
	exit 1
}

if [[ $# -ne 1 ]]; then
  _usage
fi

### Variables ###
infra_env=${1}
basename_=$(basename ${0})
servicename_=$(basename ${PWD})

# Include NFS handling variables & functions
# shellcheck source=${PWD}/nfs.sh
. ${PWD}/gitlab-ci/nfs.sh

### Program ###
echo "${basename_}: start"

if [ -d ${DEPLOY_DIR} ]; then
    cd ${DEPLOY_DIR}
    docker-compose -f docker-compose.provision.yml down --rmi all -v || true
    docker stack rm ${servicename_}-${infra_env}
    cd -
else
    echo "WARNING: Not found: ${DEPLOY_DIR}. Nothing to remove."
fi

# Delete NFS shares
sudo ${modify_exports_script_} -f ${exports_file_} -m ${nfs_base_path_}/${servicename_}-${infra_env}/app -d
sudo ${modify_exports_script_} -f ${exports_file_} -m ${nfs_base_path_}/${servicename_}-${infra_env}/certs -d
sudo exportfs -a

echo "${basename_}: end"
