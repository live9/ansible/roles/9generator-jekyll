#!/bin/bash
# Script to check if an url is reachable. The url can be set either using an env
# variable or passed as a parameter.
#
# It will try to connect to the HTTP url and will print the returned HTTP code,
# and return:
# - 0 if it is able to connect and HTTP code is not an error code,
# - 1 if it is able to connect, but it receives an error code (ie. 5xx code)
# - 0 if URL is not reachable (ie. connection timeout or refused) which means the
#   infrastructure is down. Additionally on this case will create the file
#   infra_http_code to let the infrastructure creation job in CI/CD pipeline know
#   it needs to bring it up.
#
# Author: Juan Luis Baptiste <juan.baptiste@karisma.org.co>
#

DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env

CONNECT_TIMEOUT=${CONNECT_TIMEOUT:-10}
URL=${1:-$URL}
[ "${URL}" == "" ] && echo "Host not set." && exit 1

http_code=$(curl -o /dev/null \
                 --insecure \
                 --silent \
                 --connect-timeout ${CONNECT_TIMEOUT} \
                 --head \
                 --write-out "%{http_code}" \
                 "${URL}")
echo ${http_code} > infra_http_code
if [ "${http_code}" == "000" ]; then
  echo "URL is not accesible. (HTTP code: ${http_code})"
  exit 0
elif [ "${http_code}" == "500" ]; then
  echo "URL is accessible but not working correctly. (HTTP code: ${http_code})"
  exit 1
else
  echo "URL is accesible. (HTTP code: ${http_code})"
  exit 0
fi
