#!/bin/bash
# Manipulate an NFS exports file. Update mount points options, delete them or add
# new ones.
#
# Author: Juan Luis Baptiste <juan _at_ juanbaptiste _dot_ tech>

DEBUG=${DEBUG:-0}
DELETE_LINE=0

usage()
{
cat << EOF
Manipulate an NFS exports file. Update mount points options, delete them or add
new ones.

Usage: $0 OPTIONS


OPTIONS:
-d    Delete line
-f    File to manipulate
-m    Mount point
-o    Mount options
-h    This help message
-V    Debug mode

EOF
}

while getopts df:m:o:hV option
do
  case "${option}"
  in
    d) DELETE_LINE=1
       ;;
    f) FILE_NAME="${OPTARG}"
       ;;
    m) MOUNT="${OPTARG}"
       ;;
    o) MOUNT_OPTIONS="${OPTARG}"
      ;;
    h) usage
       exit
       ;;
    V)  DEBUG=1
        ;;
    ?) usage
       exit
       ;;
  esac
done

# Validate parameters
[ ${DEBUG} -eq 1 ] && set -x
[ ${DELETE_LINE} -eq 0 ] && [ $# -lt 3 ] && usage && exit 1
[ ${DELETE_LINE} -eq 1 ] && [ $# -lt 2 ] && usage && exit 1
[ -z "${FILE_NAME}" ] && echo -e "ERROR: File to modify not set.\n" && usage && exit 1
[ -z "${MOUNT}" ] && echo -e "ERROR: Mount point not set.\n" && usage && exit 1
# only check for mount options if not deleting the mount
[ ${DELETE_LINE} -eq 0 ] && [ -z "${MOUNT_OPTIONS}" ] && echo -e "ERROR: Mount options not set.\n" && usage && exit 1

 if [ ${DELETE_LINE} -eq 1 ]; then
   echo -e "Deleting mount point: ${MOUNT}"
  sed -i "\#${MOUNT}#d" ${FILE_NAME}
 else
   grep -q "^${MOUNT}" ${FILE_NAME}
   if [ $? -eq 0 ]; then
     echo -e "Updating mount point: ${MOUNT} ${MOUNT_OPTIONS}"
     sed -i "s#^${MOUNT}.*#${MOUNT} ${MOUNT_OPTIONS}#" ${FILE_NAME}
   else
     echo -e "Adding mount point: ${MOUNT} ${MOUNT_OPTIONS}"
     sed -i "$ a\\${MOUNT} ${MOUNT_OPTIONS}" ${FILE_NAME}
   fi
 fi
