#!/bin/bash
DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env

set -Eeo pipefail
_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about line ${BASH_LINENO[0]}"
  docker logout $CI_REGISTRY
}
trap _traperr ERR

basename_=$(basename ${0})
echo "${basename_}: start"

echo "registry: doing login to $CI_REGISTRY"
docker login $CI_REGISTRY -u gitlab-ci-token -p $CI_JOB_TOKEN

echo "${basename_}: end"

