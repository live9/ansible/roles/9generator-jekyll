#!/bin/bash
DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env

set -Eeo pipefail
_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about ${BASH_LINENO[0]}"
}
trap _traperr ERR

basename_=$(basename ${0})
echo "${basename_}: start"

docker run --rm -i hadolint/hadolint < app/Dockerfile

for i in docker-*.yml; do
  # check if compose files are valid but send stdout to /dev/null. only print errors
  # to have a cleaner output
  docker-compose -f ${i} config 1> /dev/null
done

echo "${basename_}: end"
