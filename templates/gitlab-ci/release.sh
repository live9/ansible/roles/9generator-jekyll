#!/bin/bash
DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env

set -Eeo pipefail
_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about line ${BASH_LINENO[0]}"
  docker logout $CI_REGISTRY
}
trap _traperr ERR

basename_=$(basename ${0})
echo "${basename_}: start"

echo "app: pulling container image"
docker pull $CI_REGISTRY_IMAGE/app:$CI_COMMIT_SHORT_SHA
echo "app: tagging container image"
docker tag $CI_REGISTRY_IMAGE/app:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE/app
echo "app: pushing container image"
docker push $CI_REGISTRY_IMAGE/app
echo "registry: doing logout from $CI_REGISTRY"
docker logout $CI_REGISTRY

echo "${basename_}: end"
