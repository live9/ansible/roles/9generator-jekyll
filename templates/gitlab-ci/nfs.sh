#!/bin/bash

# YAML processing script taken from:
# https://github.com/jasperes/bash-yaml
# shellcheck source=${PWD}
. ${PWD}/gitlab-ci/yaml.sh

# Load YAML file contents into variables
create_variables ${PWD}/manifest.yml

modify_exports_script_="${PWD}/gitlab-ci/modify_exports.sh"
exports_file_="/etc/exports"
nfs_export_options_="*(rw,async,no_root_squash)"
# nfs_base_path_="${NFS_BASE_PATH:-/var/sites/jekyll}"
# nfs_mount_options_="${NFS_MOUNT_OPTS:-*(rw,sync,no_root_squash)}"

# Define the variable names we need to get the values from
nfs_base_path_var_name="environments_${infra_env}_nfs_app_mountpoint"
nfs_mount_opts_var_name="environments_${infra_env}_nfs_mount_opts"

# Extract the previous variables values
nfs_base_path_=${!nfs_base_path_var_name}
nfs_mount_options_=${!nfs_mount_opts_var_name}
