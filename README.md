9generator-jekyll
=========

A role to bootstrap [jekyll](https://jekyllrb.com/) projects using [9generator](https://gitlab.com/live9/ansible/playbooks/9generator).

Requirements
------------

Python 3.

Role Variables
--------------

This role depends on [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core) role, so it needs the following variables, plus any other needed by that role. This is the list of variables:

#### Mandatory

Global:

* project_type: It always must be _"jekyll"_.
* environments: Defines the infrastructure environments where this service is going to be deployed. You need to specify at least one.

Per environment:

* service_hostname: The service hostname for that environment.
* service_name: The name of the jekyll application being deployed.
* NFS configuration:
  * server_address:  NFS server address
  * app_mountpoint: Name of the NFS share to access
  * mount_opts: NFS mount options (default: nolock,soft,rw,nfsvers=4,async)

#### Optional

Global:
  * enable_resources: Enable container resource limits and reservations. (default: True)

  Per environment:

  * replicas: amount of containers to start.
  * NFS configuration:
    * mount_opts: NFS mount options (default: nolock,soft,rw,nfsvers=4,async)
  * resources: The container resources to be assigned to the _server_ container (for agent resource please edit the template file itself). Please read swarm official documentation for details on [how resource parameters work](https://docs.docker.com/config/containers/resource_constraints/). Currently we only handle memory resources. If defined, inside this block the following variables can be defined:
      * limits:
          * memory: Maximum memory the container can use. (default: "1024m")
      * reservations:
          * memory: Initial amount of memory assigned to the container. (default: "256m")
     * update_config:
       * parallelism: Maximum amount of containers to update in parallel when doing a [service update](https://docs.docker.com/engine/reference/commandline/service_update/). (default: 2)
       * delay: Delay between container updates. (default: 10s)
     * data_volume: Local path in the host to the directory to store portainer data files. (Default: "/var/compose/infra/portainer/volumes/data")


  Example `manifest.yml` file:

    ---
    # Mandatory variables
    project_type: jekyll

    # Optional variables
    file_owner_id: 1000
    # Use this variable to set a different output directory for rendered templates
    # Default is ../
    out_dir: foo

    # Template dependent variables
    service_name: my_project
    environments:
      prod:
        service_hostname: www.example.org
        nfs:
          server_address: swarm-manager-prod-1
          app_mountpoint: /var/sites/jekyll/
          mount_opts: nolock,soft,rw,nfsvers=4,async
        replicas: 2
        resources:
          limits:
            memory: "1024m"
          reservations:
            memory: "512m"
        update_config:
          parallelism: 1
          delay: 10s
      preprod:
        service_hostname: prewww.example.org
        nfs:
          server_address: swarm-manager-preprod-1
          app_mountpoint: /var/sites/jekyll/
          mount_opts: nolock,soft,rw,nfsvers=4,async
        replicas: 1
        resources:
          limits:
            memory: "512m"
          reservations:
            memory: "256m"
        update_config:
          parallelism: 1
          delay: 10s
      feature:
        service_hostname: feawww.example.org
        nfs:
          server_address: swarm-manager-preprod-1
          app_mountpoint: /var/sites/jekyll/
          mount_opts: nolock,soft,rw,nfsvers=4,async
        replicas: 1
        resources:
          limits:
            memory: "512m"
          reservations:
            memory: "256m"
        update_config:
          parallelism: 1
          delay: 10s


Dependencies
------------

* [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core)

Example Playbook
----------------

    ---
      - name: Bootstrap project
        hosts: localhost
        connection: local
        gather_facts: no
        vars:
          manifest_path: "manifest.yml"
        tasks:
          - include_vars: "{{ manifest_path }}"
          - name: Bootstrap Jekyll project
            include_role:
              name: 9generator-jekyll

License
-------

GPLv3 or later

Author Information
------------------

Juan Luis Baptiste <juan.baptiste _at_ karisma.org.co >
